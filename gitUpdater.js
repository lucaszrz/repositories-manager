const path = require("path");
const fs = require("fs");
const os = require("os");
const { execSync } = require("child_process");

/**
 * Printing a proper line break for the Operational System.
 */
const breakLine = () => {
  console.log(os.EOL);
};

breakLine();
console.log("Scanning for GIT projects:");
breakLine();

const repositories = [];

/**
 * This will search for every folder and file under startPath, trying to find a
 * '.git' folder, that means the folder * where the '.git' is, is a git repository.
 *
 * @param {Function} callback that will run for each match
 * @param {string} startPath the start path where the function will run
 */
const findPath = (callback, startPath = ".") => {
  const filter = /\.git$/;
  const files = fs.readdirSync(startPath);

  for (let i = 0; i < files.length; i++) {
    const currentPath = path.join(startPath, files[i]);
    const stat = fs.lstatSync(currentPath);

    if (filter.test(currentPath)) {
      callback(currentPath);
    } else if (stat.isDirectory()) {
      findPath(callback, currentPath);
    }
  }
};

/**
 * This will display a header for the action that will be performed in the project.
 *
 * @param {string} path to the repository
 * @param {string} headerMessage message that will be displayed before the project
 * name
 */
const showHeaderForProject = (path, headerMessage = "Project") => {
  const project = path
    .split("/")
    .filter((e) => !!e)
    .reverse()
    .shift();

  console.log(`${headerMessage} ${project}`);
  console.log("------------------------------------------------------------");
};

/**
 * This will run a command to update the git repository and pretty print the output.
 *
 * @param {string} path to the repository
 */
const updateRepository = (path) => {
  showHeaderForProject(path, "Updating project");

  // Get the current branch name to return to later.
  const currentBranch = execSync("git branch --show-current", {
    cwd: path,
  })
    .toString()
    .trim();

  // Checks if the current branch is tracking a remote.
  try {
    execSync(`git config --get branch.${currentBranch}.remote`, { cwd: path })
      .toString()
      .trim();
  } catch (error) {
    console.log(
      "\x1b[33m%s\x1b[0m",
      "This repository isn't tracking any remote."
    );

    breakLine();
    console.log();

    return;
  }

  const statusOutput = execSync("git status --short", { cwd: path }).toString();
  const isWorkingTreeClean = statusOutput === "";

  if (!isWorkingTreeClean) {
    // Move changed files to a temporarily stash.
    execSync("git stash push . --include-untracked", {
      cwd: path,
    });
  }

  if (currentBranch !== "master") {
    // Switches to the master branch.
    execSync("git switch master", { cwd: path });
  }

  // Switches to master branch and update`s it fetching all branches.
  const output = execSync("git fetch -p && git pull --all", {
    cwd: path,
  }).toString();

  if (currentBranch !== "master") {
    // Switches back to the previous branch.
    execSync(`git switch ${currentBranch}`, { cwd: path });
  }

  if (!isWorkingTreeClean) {
    // Move back changed files from stash to current branch
    execSync("git stash pop", { cwd: path });
  }

  if (output.indexOf("error") >= 0) {
    console.log("\x1b[31m%s\x1b[0m", `error: ${output}`);
    return;
  } else if (output.indexOf("Already up to date.") >= 0) {
    let splitOutput = output.split("\n");

    let prettyPrintIndex = splitOutput.findIndex(
      (i) => i === "Already up to date."
    );

    splitOutput[
      prettyPrintIndex
    ] = `\x1b[32m${splitOutput[prettyPrintIndex]}\x1b[0m`;

    console.log(splitOutput.join("\n"));
  } else {
    console.log(output);
  }

  breakLine();

  branchPruner(path);
};

/**
 * This will run a command to prune all deleted branches from the git repository
 * and pretty print the output.
 *
 * @param {string} path to the repository
 */
const branchPruner = (path) => {
  showHeaderForProject(path, "Pruning branches from");

  const pruneCommand = String.raw`git fetch -p && git branch -vv | grep ': gone]' | awk '{ print $1 }' | xargs -r git branch -d`;
  const output = execSync(pruneCommand, { cwd: path }).toString();

  if (!output) {
    console.log("\x1b[32m%s\x1b[0m", "No branches to prune.");
    breakLine();
  } else if (output.indexOf("error") >= 0) {
    console.log("\x1b[31m%s\x1b[0m", `error: ${output}`);
    breakLine();
  } else {
    console.log(output);
  }
};

/**
 * Checks if and working directory was passed by args.
 */
const argDir = process.argv.slice(2).shift() || "";

let workingDir = process.cwd();

if (argDir !== "") {
  const lstatSync = fs.lstatSync(argDir, { throwIfNoEntry: false });

  if (lstatSync && lstatSync.isDirectory()) {
    workingDir = argDir;
  }
}

/**
 * Calls the function that will find the git repositories.
 */
findPath((path = "") => {
  if (
    path.indexOf("vendor") === -1 &&
    path.indexOf("composer") === -1 &&
    path.indexOf(".vscode") === -1 &&
    path.indexOf(".idea") === -1 &&
    path.indexOf("node_modules") === -1
  ) {
    switch (process.platform) {
      case "win32":
        path = path.replace(/\//g, "\\");
        path = path.replace(".git", "");
        break;
      default:
        path = path.replace(/\\/g, "/");
        path = path.replace(".git", "");
        break;
    }

    repositories.push(path);
    console.log("\x1b[33m%s\x1b[0m", "-- found: ", path);
  }
}, workingDir);

breakLine();

/**
 * Perform git actions under each repository.
 */
repositories.forEach((repository) => {
  updateRepository(repository);
});
